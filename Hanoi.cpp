#include <iostream>
#include <stack>
#include <cstdlib>
#include <windows.h>
#include <stdio.h>

#define clearing() printf("\033[H\033[J")

using namespace std;

int N;
double S;
stack<int> A;
stack<int> B;
stack<int> C;

void printTowers(stack<int>, stack<int>, stack<int>);

void tower(int a, char from, char aux, char to){
    if(a == 1){
       int temp;
       switch(from){
            case 'A': temp = A.top(); A.pop(); break;
            case 'B': temp = B.top(); B.pop(); break;
            case 'C': temp = C.top(); C.pop(); break;
       }
       switch(to){
            case 'A': A.push(temp); break;
            case 'B': B.push(temp); break;
            case 'C': C.push(temp); break;
       }
       clearing();
       printTowers(A, B, C);
       Sleep(S);
       return;
    }
    else{
       tower(a-1, from, to, aux);
       int temp;
       switch(from){
            case 'A': temp = A.top(); A.pop(); break;
            case 'B': temp = B.top(); B.pop(); break;
            case 'C': temp = C.top(); C.pop(); break;
       }
       switch(to){
            case 'A': A.push(temp); break;
            case 'B': B.push(temp); break;
            case 'C': C.push(temp); break;
       }
       clearing();
       printTowers(A, B, C);
       Sleep(S);
       tower(a-1, aux, from, to);
    }
}

void printSpaces(int n){
    for(int i = 0; i < n; ++i)
        cout<<" ";
}

void printStars(int n){
    for(int i = 0; i < n; ++i)
        cout<<"*";
}

void printDisc(int n){
    printSpaces(N - n);
    printStars(n * 2 - 1);
    printSpaces(N - n);
}

void printTowers(stack<int> a, stack<int> b, stack<int> c){
    //2*N-1 number of spaces to print if no disc
    cout<<"\n\n";
    int sa = a.size(), sb = b.size(), sc = c.size();
    for(int i = 0; i < N; ++i){
        if(N - sa - i <= 0){ // check if discs are less than N
            printDisc(a.top());
            a.pop();
        }
        else{
            printSpaces(2 * N - 1);
        }
        printSpaces(2);

        if(N - sb - i <= 0){
            printDisc(b.top());
            b.pop();
        }
        else{
            printSpaces(2 * N - 1);
        }
        printSpaces(2);

        if(N - sc - i <= 0){
            printDisc(c.top());
            c.pop();
        }
        else{
            printSpaces(2 * N - 1);
        }
        cout<<endl;
    }
}

int main(){
     cout<<"\t\t\tTowers of Hanoi\n\n";
     cout<<"Enter number of discs : ";
     cin>>N;
     while (cin.fail() || N < 1 || N > 2147483647){
        cin.clear();
        cin.ignore(256,'\n');
        cout<<"Enter number of discs : ";
        cin >> N;
     }
     cout<<"\nEnter animation speed in seconds (decimals work as well): ";
     cin>>S;
     while (cin.fail() || S < 0.001 || S > 2147483647){
        cin.clear();
        cin.ignore(256,'\n');
        cout<<"\nEnter animation speed in seconds (decimals work as well): ";
        cin >> S;
     }
     S*=1000;
     for(int i = N; i > 0; --i){ //puts discs on pole A
        A.push(i);
     }
     clearing();
     printTowers(A, B, C);
     Sleep(S);
     tower(N, 'A', 'B', 'C');
     return 0;
}
